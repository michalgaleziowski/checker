# README #

Scripts for checking auctions without images on listings.
Scripts are checking each image link, testing for Content-Length from header, then inserting into database auctions' numbers where image is not loaded correctly.
Scripts are running with CRON with email notification.