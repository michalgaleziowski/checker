<?php
/*																																							*/
/*	Script to find wrong image links for all user on ebay.fr																								*/
/*	Script using javascript to find all images' source then passing to saveToDb.php script 																	*/
/*	Author: Michal Galeziowski																																*/

require_once('simple_html_dom/simple_html_dom.php');
require_once('saveToDb.php');

set_time_limit(0);
$scriptname = basename($_SERVER["SCRIPT_FILENAME"]);

if(!file_exists('logs')){
	mkdir('logs');
	chmod("logs", 0755);
}

																												
/* Reading url content for specified html elements */
function getLinks($url){

	$options = array(
           
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_COOKIEJAR => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_COOKIEFILE => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_SSL_VERIFYPEER => false 
            
        );
	
	$ch3 = curl_init($url);
	curl_setopt_array($ch3, $options);
	$str = curl_exec($ch3);
	curl_close($ch3);
	
	$html = new simple_html_dom();
	$html->load($str);

	$linki = array();	

		foreach ($html->find("div[class=lvpic pic img left]") as $val){
			$auction_id = $val->attr['iid'];
        	if(isset($val->children[0]->children[0]->children[0]->attr['src'])){
        		$img_ = $val->children[0]->children[0]->children[0]->attr['src'];
        		
        		if(($img_ == 'https://ir.ebaystatic.com/pictures/aw/pics/s_1x2.gif') || ($img_ == 'http://ir.ebaystatic.com/pictures/aw/pics/s_1x2.gif')){
        			if(isset($val->children[0]->children[0]->children[0]->attr['imgurl'])){
	        			$image_link = $val->children[0]->children[0]->children[0]->attr['imgurl'];
	        		}else{
	        			$image_link = $img_;
	        		}
        		}else{
        			$image_link = $img_;
        		}
        		$image_ = str_replace('s-l225', 's-l140', $image_link);
        	
        	}elseif(isset($val->children[0]->children[2]->children[0]->attr['src'])){
        		$img_ = $val->children[0]->children[2]->children[0]->attr['src'];
        		
        		if(($img_ == 'https://ir.ebaystatic.com/pictures/aw/pics/s_1x2.gif') ||  ($img_ == 'http://ir.ebaystatic.com/pictures/aw/pics/s_1x2.gif')){
        			if(isset($val->children[0]->children[2]->children[0]->attr['imgurl'])){
	        			$image_link = $val->children[0]->children[2]->children[0]->attr['imgurl'];
	        		}else{
	        			$image_link = $img_;
	        		}
        		}else{
        			$image_link = $img_;
        		}
        		$image_ = str_replace('s-l225', 's-l140', $image_link);
        	
        	}else{
        		$image_ = 'Cannot find URL';
        		
        	}
        	if(!in_array($auction_id, $linki)) $linki[$auction_id] = $image_;
        }
    return $linki;
}

function getCategories($url){
	$html = file_get_html($url);	
	foreach ($html->find("div[class=cat-link]") as $val){
		$categoriesLink[] = $val->children[0]->attr['href'];
	}
	return $categoriesLink;
}
$region = 'FR';
$user = array('grenico-fr');
foreach ($user as $user_temp) {

/* 											*/
/*	   Preparing links for each user	 	*/
/*											*/
	$links = array();
	$CatAllLink = array();

	$options = array(
           
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_COOKIEJAR => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_COOKIEFILE => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_TIMEOUT => 20,  
            
        );

	file_put_contents('logs/checkFRlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	Start for user: '.$user_temp.PHP_EOL ,FILE_APPEND);
	
	$pageLink = array('http://www.ebay.fr/sch/m.html?_ssn='.$user_temp.'&_clu=2&_fcid=71&_localstpos=75001&_stpos=75001&_gbr=1&_ipg=200&_sop=7&rt=nc');
	
/* Links to categories */
	$categoriesLinks = getCategories($pageLink[0]);
	
	$c1 = curl_init($pageLink[0]);
	curl_setopt_array($c1, $options);
	curl_exec($c1);
	file_put_contents('logs/checkFRlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.':: '.count($categoriesLinks).' categories found'.PHP_EOL, FILE_APPEND);
	foreach ($categoriesLinks as $value) {
		
		$ch = curl_init($value);
		curl_setopt_array($ch, $options);
		$page = curl_exec($ch);
		curl_close($ch);

	/* Cutting text to get quantity of items. Needed for preparing how many pages needs to be explored */			
		$indexStart = strpos($page, "<span class=\"rcnt\"");
		$string = substr($page, $indexStart, 35);
		$string = substr($string, strpos($string, '>')+1);
		$string = preg_replace('/\D/', '', $string);
		$pagesNumber = ceil((int)$string / 200);
		file_put_contents('logs/checkFRlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.':: '.$string.' items found for '.$user_temp.' in category: '.$value.PHP_EOL, FILE_APPEND);
		
		//if($string == '0') file_put_contents('qt_it_in_category.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	Cannot find items for user: '.$user_temp.PHP_EOL ,FILE_APPEND);
	/* Preparing links to each page */
		if($pagesNumber >= 1) {
			for ($i = 1; $i <= $pagesNumber; $i++){
				$temp = substr($value,0, -23);
				$new_link = substr_replace($temp, '&_sop=7&_pgn='.$i.'&_skc='. (($i*100)+($i-2)*100) . '&rt=nc',  -1);
				//file_put_contents('wygenerowalem.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	'.$new_link.PHP_EOL, FILE_APPEND);
				array_push($CatAllLink, $new_link);
			}
		}
		
		$startTime = microtime(true);
	/*	Looking for specified items in html document, then checking the img src 	*/
		foreach ($CatAllLink as $val) {
			$temp = getLinks($val);
			$links = $links + $temp; 
			
		}
		$endTime = microtime(true);
		$elapsed = $endTime - $startTime;
		$generowanie =  'Link downloading '. substr($val,0, 50) . ' time: ' . $elapsed  .' sec';
		
		file_put_contents('logs/timingFR.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.':: '.$generowanie.PHP_EOL ,FILE_APPEND);
		$CatAllLink = array();
	}
	
	$startTime = microtime(true);
	testLinks($links, $region);
	$endTime = microtime(true);
	$elapsed = $endTime - $startTime;

	$testowanie = 'Link testing for '.$user_temp . '  time: '.  $elapsed .' sec';
	file_put_contents('logs/timingFR.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.':: '.$testowanie.PHP_EOL ,FILE_APPEND);
	
	file_put_contents('logs/checkFRlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	Stop for user: '.$user_temp.PHP_EOL ,FILE_APPEND);
	file_put_contents('logs/checkFRlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	User change'.PHP_EOL ,FILE_APPEND);
}
 
file_put_contents('logs/checkFRlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	Stop. FR Done'.PHP_EOL ,FILE_APPEND);

?>

