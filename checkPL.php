<?php
/*																																							*/
/*	Script to find wrong image links for all user on allegro.pl																							*/
/*	Script using javascript to find all images' source then passing to saveToDb.php script 																	*/
/*	Author: Michal Galeziowski																																*/

require_once('simple_html_dom/simple_html_dom.php');
require_once('saveToDbDev.php');

set_time_limit(0);
$scriptname = basename($_SERVER["SCRIPT_FILENAME"]);

/*if(!file_exists('logs')){
	mkdir('logs');
	chmod("logs", 0755);
}
*/
																												
/* Reading url content for specified html elements */
function getLinks($url){

	$options = array(
           
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_COOKIEJAR => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_COOKIEFILE => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_SSL_VERIFYPEER => false 
            
        );
	
	$ch3 = curl_init($url);
	curl_setopt_array($ch3, $options);
	$str = curl_exec($ch3);
	curl_close($ch3);
	
	$html = new simple_html_dom();
	$html->load($str);

	$linki = array();	

		foreach ($html->find("article[class=fa72b28]") as $val){
			$auction_id = $val->children[0]->children[0]->children[1]->children[0]->attr['href'];
			$auction_id = substr($auction_id, -15);
			$auction_id = substr($auction_id, 0, -5);
			//$auction_id = $val->attr['iid'];
        	if(isset($val->children[0]->children[0]->children[0]->children[0]->children[0]->children[0]->attr['src'])){
        		$img_ = $val->children[0]->children[0]->children[0]->children[0]->children[0]->children[0]->attr['src'];
        		$image_link = $img_;
        		
        	}else{
        		$image_link = 'Cannot find URL';
        		
        	}
        	if(!in_array($auction_id, $linki)) $linki[$auction_id] = $image_link;
        }
    return $linki;
}


$region = 'PL';
$user = array('9578341');
foreach ($user as $user_temp) {

/* 											*/
/*	   Preparing links for each user	 	*/
/*											*/
	$links = array();
	$CatAllLink = array();

	$options = array(
           
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_COOKIEJAR => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_COOKIEFILE => dirname(__FILE__) . '/cookie.txt',
            CURLOPT_SSL_VERIFYPEER => false  
            
        );

	file_put_contents('logs/checkPLlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	Start for user: '.$user_temp.PHP_EOL ,FILE_APPEND);	
	$pageLink = array('http://allegro.pl/listing/user/listing.php?us_id='.$user_temp.'&order=n&id=3');
	
	$c1 = curl_init($pageLink[0]);
	curl_setopt_array($c1, $options);
	$page = curl_exec($c1);

	/* Cutting text to get quantity of items. Needed for preparing how many pages needs to be explored */			
		$indexStart = strpos($page, "<span class=\"user-info__counter-value\"");
		$string = substr($page, $indexStart, 70);
		$string = substr($string, strpos($string, '>')+1);
		$string = preg_replace('/\D/', '', $string);
		$pagesNumber = ceil((int)$string / 60);	
		
	/* Preparing links to each page */
		if($pagesNumber >= 1) {
			for ($i = 1; $i <= $pagesNumber; $i++){
				$temp = substr($pageLink[0], count($pageLink[0])-1);
				$new_link = substr_replace($temp, '&p='.$i.'&id=3',  -1);
				array_push($CatAllLink, $new_link);
			}
		}
		
		$startTime = microtime(true);
	/*	Looking for specified items in html document, then checking the img src 	*/
		foreach ($CatAllLink as $val) {
			$temp = getLinks($val);
			$links = $links + $temp; 	
		}

		//var_dump($links);
		$endTime = microtime(true);
		$elapsed = $endTime - $startTime;
		$generowanie =  'Link downloading '. substr($val,0, 50) . ' time: ' . $elapsed  .' sec';
		
		file_put_contents('logs/timingPL.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.':: '.$generowanie.PHP_EOL ,FILE_APPEND);
		$CatAllLink = array();
	//}
	
	$startTime = microtime(true);
	testLinks($links, $region);
	$endTime = microtime(true);
	$elapsed = $endTime - $startTime;

	$testowanie = 'Link testing for '.$user_temp . '  time: '.  $elapsed .' sec';
	file_put_contents('logs/timingPL.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.':: '.$testowanie.PHP_EOL ,FILE_APPEND);
	
	file_put_contents('logs/checkPLlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	Stop for user: '.$user_temp.PHP_EOL ,FILE_APPEND);
	file_put_contents('logs/checkPLlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	User change'.PHP_EOL ,FILE_APPEND);
}
 
file_put_contents('logs/checkPLlog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	Stop. PL Done'.PHP_EOL ,FILE_APPEND);

?>

