<?php
/*																																							*/
/*	Script checking received array, testing all links. Then checking Response Header for Content-Length. 													*/	
/*	If Content-Length equals 49 or 1359, it means that image is default. Pushing into database all auctions id where image has not been set as expected		*/
/*	Author: Michal Galeziowski																																*/
/*																																							*/

	function testLinks($receivedArray, $region){
	/* Database login details */
		$login_info = parse_ini_file('configForImageCheck.ini');
		$host = $login_info['host'];
		$port = $login_info['port'];
		$pass = $login_info['pass'];
		$user = $login_info['user'];
		$scriptname = basename($_SERVER["SCRIPT_FILENAME"]);
	
		switch ($region){
			case 'PL':
				$database = 'allegro_auctions_without_img';
				$default_image_size = 530;
				$default_image_size2 = 530;
				$default_image_size3 = 530;
			default :
				$database = 'ebay_auctions_without_img';
				$default_image_size = 49;
				$default_image_size2 = 1359;
				$default_image_size3 = -1;
		}

		$toInsert = array();
		$val = '';

		 $options = array(
		        CURLOPT_RETURNTRANSFER => true,     // return web page
		        CURLOPT_HEADER         => true,    // don't return headers
		        CURLOPT_NOBODY 		   => true,
		        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
		        CURLOPT_TIMEOUT        => 120,  	// Disabled SSL Cert checks    // timeout on response
		        CURLOPT_SSL_VERIFYPEER => false 
   		 );

		try{
			$pdo = new PDO('mysql:host='.$host.';dbname=;port='.$port.'',$user,$pass);

	// Testing each link for Content-Length */
			foreach ($receivedArray as $key => $value) {
				
				$ch1 = curl_init($value);
				curl_setopt_array($ch1, $options);
				curl_exec($ch1);
				$test = curl_getinfo($ch1, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
			
				if(($test == $default_image_size) || ($test == $default_image_size2) || ($test == $default_image_size3)){		
					if(!in_array($key, $toInsert))	$toInsert[] = $key;
					
				}else{
					// do nothing
				}
			}

	 //Preparing data for INSERT query 
			foreach ($toInsert as $value) {
				$val = $val.',('.$value.')';
			}
			$val = substr($val, 1);
			if($val != ''){
				$query = 'INSERT INTO michalg.'.$database.'(auction_id) VALUES '.$val;
				file_put_contents('logs/sqlquery.txt','['.date("d/M/Y H:i:s").'] '. $query.PHP_EOL, FILE_APPEND);
				$insert_query = $pdo->prepare($query);
				if($insert_query->execute()){
					file_put_contents('logs/saveToDbLog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	Running '.$query.'...'.PHP_EOL ,FILE_APPEND);
					file_put_contents('logs/saveToDbLog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	DONE '.PHP_EOL ,FILE_APPEND);
				}else{
					file_put_contents('logs/saveToDbLog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	Something gone wrong with: '.$query.'...'.PHP_EOL ,FILE_APPEND);
				}
				file_put_contents('logs/saveToDbLog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	DONE '.PHP_EOL ,FILE_APPEND);
			}else{
				file_put_contents('logs/saveToDbLog.txt', '['.date("d/M/Y H:i:s").'] '.'//'.$scriptname.'::	There is nothing to insert.'.PHP_EOL ,FILE_APPEND);	

			}
	}catch(PDOException $e){
			file_put_contents('logs/saveToDbLog.txt', $e.PHP_EOL ,FILE_APPEND);
		}
	}
?>